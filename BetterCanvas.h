#pragma once
#include "Circle.h"
#include "Canvas.h"
#include "Rectangle.h"
#include "Arrow.h"
#include "Triangle.h"

class BetterCanvas
{
public:

	// Constructor
	BetterCanvas();

	// Destructor
	~BetterCanvas();

	void drawShape(const Shape& shape, const Color color);
	void clearShape(const Shape& shape);
private:
	Canvas canvas;
};
