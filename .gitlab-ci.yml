image: alpine

###################################
# gitlab-ci.yml for Ex5 - Shapes  #
###################################

stages:
  - build-pipeline-tools
  - files-verifier
  - part1Answers
  - build1
  - test1
  - build-memory
  - test-memory
  - build-bonus
  - test-bonus
  - submission-date-verifier
  - zip-and-send

Build Pipeline Tools:
  stage: build-pipeline-tools
  before_script:
    - apk update
    - apk add git
    - apk add go
    - apk add build-base gcc abuild binutils

  script:

  # build utils
    - git clone https://gitlab.com/exercisetests/utils.git
    - cd utils
    - g++ clearscreen.cpp -o clearscreen
    - g++ printbanner.cpp -o printbanner
    - g++ printMessage.cpp -o printMessage
    
    - mv clearscreen ..
    - mv printbanner ..
    - mv printMessage ..
    - mv CodesToMessages.csv ..
    - cd ..

  # build files verifier
    - git clone https://gitlab.com/exercisetests/verifysubmittedfiles.git
    - cd verifysubmittedfiles
    - go build .
    - mv filesverifier ..
    - cd ..

  # build memory verifier
    - git clone https://gitlab.com/exercisetests/valgrindvarifier.git
    - cd valgrindvarifier
    - g++ ValgrindVarifier.cpp -o valgrindverifier
    - mv valgrindverifier ..
    - cd ..

  # build ex5 tests
    - git clone https://gitlab.com/exercisetests/ex5.git
    - cd ex5
    - mv PreSubmissionChecker.txt ..
    - mv PostSubmissionChecker.txt ..
    - mv Part1AChecker.txt ..
    - mv Part1BChecker.txt ..
    - mv Part1CChecker.txt ..
    - mv Part1DChecker.txt ..
    - mv Part1EChecker.txt ..
    - mv BonusChecker.txt ..
    - mv MemoryChecker.txt ..
    - cd ..
  artifacts:
    paths:
      - filesverifier
      - valgrindverifier
      - clearscreen
      - printMessage
      - printbanner
      - CodesToMessages.csv
      - PreSubmissionChecker.txt
      - PostSubmissionChecker.txt
      - Part1AChecker.txt
      - Part1BChecker.txt
      - Part1CChecker.txt
      - Part1DChecker.txt
      - Part1EChecker.txt
      - BonusChecker.txt
      - MemoryChecker.txt

Verify Submission:
  stage: files-verifier
  needs:
    - job: Build Pipeline Tools
  script:
    - echo "Verifying .gitignore..."
    - ./filesverifier PreSubmissionChecker.txt 5
    - echo "Gitignore files exists"
    - echo "Verifying required VS files..."
    - ./filesverifier PreSubmissionChecker.txt 2
    - echo "Required VS files ok"
    - echo "Verifying files that should not be submitted..."
    - ./filesverifier PreSubmissionChecker.txt 4
    - echo "Excluded files ok"

Compile Part 1a:
  stage: build1
  needs:
    - job: Build Pipeline Tools
  before_script:
  - apk add git
  - apk add build-base gcc abuild binutils
  script:
    - ./clearscreen 50
    - ./printbanner
    - echo "Verifying part1a files exist..."
    - ./filesverifier Part1AChecker.txt 1
    - echo "Part 1a - Point --- files ok"
    - echo "Compiling ..."
    - git clone https://gitlab.com/exercisetests/ex5.git
    - cd ex5
    - mv Ex5Part1ATest.cpp ..
    - cd ..
    - cd temp123
    - for f in *; do cp -avf "$f" ../"$f"; done;
    - cd ..
    - g++ Point.cpp Ex5Part1ATest.cpp -o exec1a
    - echo "Ex5 Part 1a compiled successfully"

  artifacts:
    paths:
      - exec1a

Part 1a tests:
  stage: test1
  needs:
    - job: Build Pipeline Tools
    - job: Compile Part 1a
  before_script:
    - apk update
    - apk add build-base gcc abuild binutils
    - apk add git
  script:
    - ./clearscreen 50
    - ./printbanner
    - ./exec1a

Compile Part 1b:
  stage: build1
  needs:
    - job: Build Pipeline Tools
  before_script:
  - apk add git
  - apk add build-base gcc abuild binutils
  - apk add libx11-dev
  script:
    - ./clearscreen 50
    - ./printbanner
    - echo "Verifying part1b files exist..."
    - ./filesverifier Part1BChecker.txt 1
    - echo "Part 1b - Arrow --- files ok"
    - echo "Compiling ..."
    - git clone https://gitlab.com/exercisetests/ex5.git
    - cd ex5
    - mv Ex5Part1BTest.cpp ..
    - cd ..
    - cd temp123
    - for f in *; do cp -avf "$f" ../"$f"; done;
    - cd ..
    - g++ Point.cpp Shape.cpp Arrow.cpp Canvas.cpp Ex5Part1BTest.cpp -lpthread -lX11 -o exec1b
    - echo "Ex5 Part 1b compiled successfully"

  artifacts:
    paths:
      - exec1b

Part 1b tests:
  stage: test1
  needs:
    - job: Build Pipeline Tools
    - job: Compile Part 1b
  before_script:
    - apk update
    - apk add build-base gcc abuild binutils
    - apk add libx11-dev
  script:
    - ./clearscreen 50
    - ./printbanner
    - ./exec1b

# Part 1b - memory check:
#   stage: test1
#   needs:
#     - job: Compile Part 1b
#     - job: Part 1b tests
#     - job: Build Pipeline Tools
#   before_script:
#     - apk update
#     - apk add build-base gcc abuild binutils
#     - apk add cmake extra-cmake-modules
#     - apk add valgrind
#   script:
#     - echo "Looking for memory leaks..."
#     - valgrind --leak-check=yes --log-file=valgrind-out.txt ./exec1b
#     - ./valgrindverifier valgrind-out.txt
#   allow_failure: false

Compile Part 1c:
  stage: build1
  needs:
    - job: Build Pipeline Tools
  before_script:
  - apk add git
  - apk add build-base gcc abuild binutils
  - apk add libx11-dev
  script:
    - ./clearscreen 50
    - ./printbanner
    - echo "Verifying part1c files exist..."
    - ./filesverifier Part1CChecker.txt 1
    - echo "Part 1c - Circle --- files ok"
    - echo "Compiling ..."
    - git clone https://gitlab.com/exercisetests/ex5.git
    - cd ex5
    - mv Ex5Part1CTest.cpp ..
    - cd ..
    - cd temp123
    - for f in *; do cp -avf "$f" ../"$f"; done;
    - cd ..
    - g++ Point.cpp Shape.cpp Circle.cpp Canvas.cpp Ex5Part1CTest.cpp -lX11 -o exec1c
    - echo "Ex5 Part 1c compiled successfully"

  artifacts:
    paths:
      - exec1c

Part 1c tests:
  stage: test1
  needs:
    - job: Build Pipeline Tools
    - job: Compile Part 1c
  before_script:
    - apk update
    - apk add build-base gcc abuild binutils
    - apk add libx11-dev
  script:
    - ./clearscreen 50
    - ./printbanner
    - ./exec1c

# Part 1c - memory check:
#   stage: test1
#   needs:
#     - job: Compile Part 1c
#     - job: Part 1c tests
#     - job: Build Pipeline Tools
#   before_script:
#     - apk update
#     - apk add build-base gcc abuild binutils
#     - apk add cmake extra-cmake-modules
#     - apk add valgrind
#   script:
#     - echo "Looking for memory leaks..."
#     - valgrind --leak-check=yes --log-file=valgrind-out.txt ./exec1c
#     - ./valgrindverifier valgrind-out.txt
#   allow_failure: false

Compile Part 1d:
  stage: build1
  needs:
    - job: Build Pipeline Tools
  before_script:
  - apk add git
  - apk add build-base gcc abuild binutils
  - apk add libx11-dev
  script:
    - ./clearscreen 50
    - ./printbanner
    - echo "Verifying part1d files exist..."
    - ./filesverifier Part1DChecker.txt 1
    - echo "Part 1d - Triangle --- files ok"
    - echo "Compiling ..."
    - git clone https://gitlab.com/exercisetests/ex5.git
    - cd ex5
    - mv Ex5Part1DTest.cpp ..
    - cd ..
    - cd temp123
    - for f in *; do cp -avf "$f" ../"$f"; done;
    - cd ..
    - g++ Point.cpp Shape.cpp Polygon.cpp Triangle.cpp Canvas.cpp Ex5Part1DTest.cpp -lX11 -o exec1d
    - echo "Ex5 Part 1d compiled successfully"

  artifacts:
    paths:
      - exec1d

Part 1d tests:
  stage: test1
  needs:
    - job: Build Pipeline Tools
    - job: Compile Part 1d
  before_script:
    - apk update
    - apk add build-base gcc abuild binutils
    - apk add libx11-dev
  script:
    - ./clearscreen 50
    - ./printbanner
    - ./exec1d

# Part 1d - memory check:
#   stage: test1
#   needs:
#     - job: Compile Part 1d
#     - job: Part 1d tests
#     - job: Build Pipeline Tools
#   before_script:
#     - apk update
#     - apk add build-base gcc abuild binutils
#     - apk add cmake extra-cmake-modules
#     - apk add valgrind
#   script:
#     - echo "Checking for memory issues..."
#     - valgrind --leak-check=yes --log-file=valgrind-out.txt ./exec1d
#     - ./valgrindverifier valgrind-out.txt
#   allow_failure: false

Compile Part 1e:
  stage: build1
  needs:
    - job: Build Pipeline Tools
  before_script:
  - apk add git
  - apk add build-base gcc abuild binutils
  - apk add libx11-dev
  script:
    - ./clearscreen 50
    - ./printbanner
    - echo "Verifying part1e files exist..."
    - ./filesverifier Part1EChecker.txt 1
    - echo "Part 1e - Rectangle --- files ok"
    - echo "Compiling ..."
    - git clone https://gitlab.com/exercisetests/ex5.git
    - cd ex5
    - mv Ex5Part1ETest.cpp ..
    - cd ..
    - cd temp123
    - for f in *; do cp -avf "$f" ../"$f"; done;
    - cd ..
    - g++ Point.cpp Shape.cpp Polygon.cpp Rectangle.cpp Canvas.cpp Ex5Part1ETest.cpp -lX11 -o exec1e
    - echo "Ex5 Part 1e compiled successfully"

  artifacts:
    paths:
      - exec1e

Part 1e tests:
  stage: test1
  needs:
    - job: Build Pipeline Tools
    - job: Compile Part 1e
  before_script:
    - apk update
    - apk add build-base gcc abuild binutils
    - apk add libx11-dev
  script:
    - ./clearscreen 50
    - ./printbanner
    - ./exec1e

# Part1e - memory check:
#   stage: test1
#   needs:
#     - job: Compile Part 1e
#     - job: Part 1e tests
#     - job: Build Pipeline Tools
#   before_script:
#     - apk update
#     - apk add build-base gcc abuild binutils
#     - apk add cmake extra-cmake-modules
#     - apk add valgrind
#   script:
#     - echo "Looking for memory leaks..."
#     - valgrind --leak-check=yes --log-file=valgrind-out.txt ./exec1e 
#     - ./valgrindverifier valgrind-out.txt
#   allow_failure: false

Verify Necessary Exercise Files:
  stage: files-verifier
  needs:
    - job: Build Pipeline Tools
  before_script:
  - apk add git
  - apk add build-base gcc abuild binutils
  script:
    - ./clearscreen 50
    - ./printbanner
    - ./filesverifier PostSubmissionChecker.txt 1
  allow_failure: false

Compile Memory Test:
  stage: build-memory
  needs:
    - job: Build Pipeline Tools
  before_script:
  - apk add git
  - apk add build-base gcc abuild binutils
  - apk add libx11-dev
  script:
    - ./clearscreen 50
    - ./printbanner
    - echo "Verifying all shapes files exist..."
    - ./filesverifier MemoryChecker.txt 1
    - echo "Memory test --- files ok"
    - echo "Compiling ..."
    - git clone https://gitlab.com/exercisetests/ex5.git
    - cd ex5
    - mv Ex5MemoryTest.cpp ..
    - cd ..
    - cd temp123
    - for f in *; do cp -avf "$f" ../"$f"; done;
    - cd ..
    - g++ Point.cpp Shape.cpp Polygon.cpp Arrow.cpp Circle.cpp Triangle.cpp Rectangle.cpp Canvas.cpp Ex5MemoryTest.cpp -lX11 -o execmem
    - echo "Ex5 memory test compiled successfully"
  
  artifacts:
    paths:
      - execmem

Test Memory check:
  stage: test-memory
  needs:
    - job: Build Pipeline Tools
    - job: Compile Memory Test
  before_script:
    - apk update
    - apk add build-base gcc abuild binutils
    - apk add cmake extra-cmake-modules
    - apk add valgrind
    - apk add libx11-dev
  script:
    - ./clearscreen 50
    - ./printbanner
    - echo "Looking for memory leaks..."
    - valgrind --leak-check=yes --log-file=valgrind-out.txt ./execmem
    - ./valgrindverifier valgrind-out.txt
  allow_failure: true

Compile Bonus:
  stage: build-bonus
  needs:
    - job: Build Pipeline Tools
  before_script:
  - apk add git
  - apk add build-base gcc abuild binutils
  - apk add libx11-dev
  script:
    - ./clearscreen 50
    - ./printbanner
    - echo "Verifying bonus files exist..."
    - ./filesverifier BonusChecker.txt 1
    - echo "Bonus --- files ok"
    - echo "Compiling ..."
    - git clone https://gitlab.com/exercisetests/ex5.git
    - cd ex5
    - mv Ex5BonusTest.cpp ..
    - cd ..
    - cd temp123
    - for f in *; do cp -avf "$f" ../"$f"; done;
    - cd ..
    - g++ Point.cpp Shape.cpp Polygon.cpp Rectangle.cpp Quadrangle.cpp Canvas.cpp Ex5BonusTest.cpp -lX11 -o execbonus
    - echo "Ex5 Bonus Test compiled successfully"
  allow_failure: true

  artifacts:
    paths:
      - execbonus

Bonus tests:
  stage: test-bonus
  needs:
    - job: Build Pipeline Tools
    - job: Compile Bonus
  before_script:
    - apk update
    - apk add build-base gcc abuild binutils
    - apk add libx11-dev
  script:
    - ./clearscreen 50
    - ./printbanner
    - if [ ! -f execbonus ]; then echo "Bonus not found or not compiled successfully! Skipping..."; exit 0; fi
    - ./execbonus

Bonus - memory check:
  stage: test-bonus
  needs:
    - job: Build Pipeline Tools
    - job: Compile Bonus
    - job: Bonus tests
  before_script:
    - apk update
    - apk add build-base gcc abuild binutils
    - apk add cmake extra-cmake-modules
    - apk add valgrind
    - apk add libx11-dev
  script:
    - ./clearscreen 50
    - ./printbanner
    - if [ ! -f execbonus ]; then echo "Bonus not found or not compiled successfully! Skipping..."; exit 0; fi
    - echo "Looking for memory leaks..."
    - valgrind --leak-check=yes --log-file=valgrind-out.txt ./execbonus
    - ./valgrindverifier valgrind-out.txt

Verify Submission Date:
  stage: submission-date-verifier
  needs: []
  before_script:
    - apk update
    - apk add git
    - apk add go
  script:
    - git clone https://gitlab.com/exercisetests/submissionverifier.git
    - cd submissionverifier
    - mv submission_verifier.go ..
    - cd ..
    - git clone https://gitlab.com/exercisetests/submissionverifierfiles.git
    - mv submission_verifier.go submissionverifierfiles
    - cd submissionverifierfiles
    - go run submission_verifier.go $CI_PROJECT_PATH $GITLAB_USER_ID
  allow_failure: true

ZIPandSend:
  stage: zip-and-send
  needs:
    - Build Pipeline Tools
    - Verify Submission
    - Verify Necessary Exercise Files
    - Verify Submission Date
    - Compile Part 1a
    - Compile Part 1b
    - Compile Part 1c
    - Compile Part 1d
    - Compile Part 1e
    - Compile Bonus
    - Compile Memory Test
    - Part 1a tests
    - Part 1b tests
    - Part 1c tests
    - Part 1d tests
    - Part 1e tests
    - Bonus tests
    - Test Memory check
    - Bonus - memory check
  before_script:
    - apk update
    - apk add git
    - apk add go
    - apk add zip
    - apk add build-base gcc abuild binutils
  script:
    - commit_message="${CI_COMMIT_MESSAGE//$'\n'/}"
    - echo "$commit_message"
    - if [ "$commit_message" != "ING" ]; then echo "Skipping zip load..." && exit 0; fi
    - git clone https://gitlab.com/exercisetests/utils.git
    - cat utils/ING
    - export ING_SERVER_URL=$(grep "ING_SERVER_URL=" utils/ING | cut -d '=' -f2)
    - export ZIP_TARGET_SERVER=$(grep "ZIP_TARGET_SERVER=" utils/ING | cut -d '=' -f2)
    - echo "Extracted ING_SERVER_URL is:" $ING_SERVER_URL
    - echo "Extracted ZIP_TARGET_SERVER is:" $ZIP_TARGET_SERVER
    - MODIFIED_ZIP_TARGET_SERVER=${ZIP_TARGET_SERVER//\//@@@}
    - echo "Original ZIP_TARGET_SERVER is:" $ZIP_TARGET_SERVER
    - echo "Modified ZIP_TARGET_SERVER is:" $MODIFIED_ZIP_TARGET_SERVER
    - zip -r loadedFromPipeline.zip . -x ".git"
    - export STUDENT_NAME=$(basename $(dirname $(pwd)))
    - export CLASS_NAME=$(basename $(dirname $(dirname $(pwd))))
    - export PROJECT_NAME=$(basename $(pwd))
    - echo $CLASS_NAME/$STUDENT_NAME/$PROJECT_NAME/$CI_PROJECT_ID/$CI_PIPELINE_ID
    - echo sending files to...
    - echo $ING_SERVER_URL/push_project_zip/$CLASS_NAME/$STUDENT_NAME/$PROJECT_NAME/$CI_PROJECT_ID/$CI_PIPELINE_ID/$MODIFIED_ZIP_TARGET_SERVER
    - curl -X POST -F "file=@loadedFromPipeline.zip" $ING_SERVER_URL/push_project_zip/$CLASS_NAME/$STUDENT_NAME/$PROJECT_NAME/$CI_PROJECT_ID/$CI_PIPELINE_ID/$MODIFIED_ZIP_TARGET_SERVER
  allow_failure: true
  when: always
