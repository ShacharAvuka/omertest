#include "Point.h"
#include <math.h>

Point::Point(): _x(0), _y(0)
{
}

Point::Point(const double x, const double y)
{
	_x = x;
	_y = y;
}

Point::~Point()
{}

Point Point::operator+(const Point& other) const
{
	Point point(_x + other.getX(), _y + other.getY());
	return point;
}

Point& Point::operator+=(const Point & other)
{
	_x += other.getX();
	_y += other.getY();

	return *this;
}

double Point::getX() const
{
	return _x;
}

double Point::getY() const
{
	return _y;
}

double Point::distance(const Point& other) const
{	
	// Calculate correct distance.
	return sqrt( pow(_x - other.getX(), 2) + pow(_y - other.getY(), 2));
}
