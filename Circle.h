#pragma once

#include "Shape.h"
#include "Point.h"
#include <cmath>
#define PI 3.14


class Circle : public Shape
{
public:
	
	// Constructor
	Circle(const Point center, const double radius, const std::string type, const std::string name);
	
	// Destructor
	~Circle();

	// Getters
	const Point getCenter() const;
	double getRadius() const;

	// Methods
	double getPerimeter() const override;
	double getArea() const override;
	void move(const Point& other) override; // add the Point to all the points of shape

	void draw(const Canvas& canvas) override;
	void clearDraw(const Canvas& canvas) override;

private:
	Point _center;
	double _radius;
};