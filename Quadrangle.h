#pragma once
#include "Polygon.h"
class Quadrangle : public Polygon
{
public:
	// Constructor
	Quadrangle(const Point a, const Point b, const Point c, const Point d, const std::string type, const std::string name);

	// Destructor
	~Quadrangle() = default;

	// Methods
	double getArea() const override;

	virtual void draw(const Canvas& canvas);
	virtual void clearDraw(const Canvas& canvas);
};

